#!/usr/bin/python3

# Developed by Muhammad Ashfaq
# version 1.8
import requests
import csv as csvInputFile
import csv as csvMaxAdid
import csv as csvOutPutFile
import io
import os
from lib.solr_search_logic import SolrSearchLogic
from lib.logger import Logger

# Function compares two values and result is returned to the caller
def compValues(v1,v2):
    flag = False
    if not v1:
        flag = False
    elif not v2:
        flag = False
    elif v1 == v2:
        flag = True
    elif v1 != v2:
        flag = False
    return flag

# this function performs truncation of the needless trailing microsends for time
def timetrunc(tmp):
    timepatrn=''
    values = [x.strip() for x in tmp.split('T')]
    i = 0
    for v in values:
        if i == 0:
            timepatrn = v
            timepatrn = timepatrn + " "
        elif i == 1:
            timepatrn = timepatrn + v[0:-5]
            timepatrn = timepatrn
        i = i + 1
    return timepatrn

# this method converts time to string format
def strtime(str):
    tmp = str.__str__()
    return tmp[0:-7]

# This function call parse the instance string (e.g. [ instance:1-abfdg444] )  for ID and returned to caller
def parseInstanceString(str):
    sub=""
    sub = str.__str__() + ""
    tmp = sub[1:-1]
    sub = tmp.split(':')
    return sub[1]

Logger().log_and_slack("Starting solr-search-terms script", '#adwords')
Logger().log_and_slack("Step 1.  Initializing Parameters ...")
# Set of Lists for storing input data from input URL
reader = []
Products = []
SearchTerm1 = []
SearchTerm2 = []
SearchTerm3 = []
SearchTerm4 = []
NegativeProducts = []
SearchTerm1_Type = []
SearchTerm2_Type = []
SearchTerm3_Type = []
SearchTerm4_Type = []
AllSearchTerms = []
AllSearchTermsTypes = []

# Set of parameters for input and output to store input URL and output CSV file for result respectively
tmp = []
ExcelReady = []
Result = ""
rowCount = 1
rownumberCount = 0
CSVFileInput = 'input_url.txt'
CSVFileOutput = 'source_urls.csv'


# Set of parameters to find total number of combination / subsets for given columns product list
itemsize = 0
Subset = []
UniqueSets = []
lst = []
PComb = []
sublist = []

# Set of parameters for sending request to input URL / google spreadsheet
sio = []
headers = {}
headers["User-Agent"] = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:22.0) Gecko/20100101 Firefox/22.0"
headers["DNT"] = "1"
headers["Accept"] = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
headers["Accept-Encoding"] = "deflate"
headers["Accept-Language"] = "en-US,en;q=0.5"

# Set URLs for Input and Max AD_ID  reading from local text file
url_InputSource = "https://docs.google.com/spreadsheets/d/1LspvJVaNHbOE8_AWGBaIMB4mmiw5i1CbBKsI640J1Ic/"
url_MaxID = "https://docs.google.com/spreadsheets/d/17DiVgPK0LtDLbpggfxGa0dNSiMXmwfTIDVs2s-HoPos/"

# extract the URL from input_url.txt downloaded from S3
with open(CSVFileInput) as f:
    count=0
    for line in f:
        urrl = [x.strip() for x in line.split('=')]
        if count == 0 :
            url_InputSource = urrl[1]
            count = count + 1
        if count == 1 :
            url_MaxID = urrl[1]

# Calling Input CSV URL (google spreadsheet) to extract data and stored in reader
Logger().log_and_slack("Step 2.  Calling Input CSV URL (google spreadsheet) to extract data.")
Logger().log_and_slack("Step 2.1  Sheet: {}".format(url_InputSource), '#adwords')
tmpurl=[x.strip() for x in url_InputSource.split('/')]
file_id = tmpurl[5]
url = url_InputSource + "export?format=csv".format(file_id)
r = requests.get(url)
sio = io.StringIO(r.text, newline=None)
reader = csvInputFile.reader(sio, dialect=csvInputFile.excel)

# Parsing Data from Input URL source stored in reader above into required format e.g. columns
Logger().log_and_slack("Step 3.  Parsing Data from Input URL source.")
for row in reader:
    if rownumberCount > 0 :
        i = 0
        for col in row:
            if i == 0   and    col is not None:
                Products.append(col)
            elif i == 1   and    col is not None:
                SearchTerm1.append(col)
            elif i == 2   and    col is not None:
                SearchTerm2.append(col)
            elif i == 3   and    col is not None:
                SearchTerm3.append(col)
            elif i == 4   and    col is not None:
                SearchTerm4.append(col)
            elif i == 5   and    col is not None:
                NegativeProducts.append(col)
            elif i == 6   and    col is not None:
                SearchTerm1_Type.append(col)
            elif i == 7   and    col is not None:
                SearchTerm2_Type.append(col)
            elif i == 8   and    col is not None:
                SearchTerm3_Type.append(col)
            elif i == 9   and    col is not None:
                SearchTerm4_Type.append(col)
            i = i + 1;
    rownumberCount = rownumberCount + 1

# cleaning the data list column for any None or empty valyes
Logger().log_and_slack("Step 4.  Cleaning data for NULL records.")
SearchTerm1_Type=' '.join(SearchTerm1_Type).split()
SearchTerm2_Type=' '.join(SearchTerm2_Type).split()
SearchTerm3_Type=' '.join(SearchTerm3_Type).split()
SearchTerm4_Type=' '.join(SearchTerm4_Type).split()

NegativeProducts=' '.join(NegativeProducts).split(',')

# adding the SearchTerm into a consolidated list along their Types
for each in SearchTerm1:
    if each:
        AllSearchTerms.append(each)
        AllSearchTermsTypes.append(SearchTerm1_Type[0])

# adding the SearchTerm into a consolidated list along their Types
for each in SearchTerm2:
    if each:
        AllSearchTerms.append(each)
        AllSearchTermsTypes.append(SearchTerm2_Type[0])

# adding the SearchTerm into a consolidated list along their Types
for each in SearchTerm3:
    if each:
        AllSearchTerms.append(each)
        AllSearchTermsTypes.append(SearchTerm3_Type[0])

# adding the SearchTerm into a consolidated list along their Types
for each in SearchTerm4:
    if each:
        AllSearchTerms.append(each)
        AllSearchTermsTypes.append(SearchTerm4_Type[0])

# Initializing request for Max AD_ID from target CSV (google spreadsheet / sample source_url.csv) URL
# result set is stored in red variable
Logger().log_and_slack("Step 5.  Initializing request for Max AD_ID from target CSV (google spreadsheet / sample source_url.csv) URL.")
tmpurl=[x.strip() for x in url_MaxID.split('/')]
file_id = tmpurl[5]
url = url_MaxID + "export?format=csv".format(file_id)
r = requests.get(url)
ssio = io.StringIO(r.text, newline=None)
red = csvMaxAdid.reader(ssio, dialect=csvMaxAdid.excel)

# getting the Max ID from the red variable and converting int value
tmm = -1
i = 0
for rr in red:
    i = 0
    for cc in rr:
        if i == 1 :
            tmm = cc
        i = i + 1

rowCount = int( tmm )
rowCount = rowCount + 1

Logger().log_and_slack("Step 6.  Max Ad_ID : has been read from target URL : {} ".format(rowCount))

Logger().log_and_slack("Step 7.  Initiating Cross-Join.")
ExcelReady = SolrSearchLogic().cross_join(AllSearchTerms, Products, rowCount, AllSearchTermsTypes, NegativeProducts, SearchTerm1, SearchTerm2, SearchTerm3, SearchTerm4, tmm)

# Writing Ouput of above corss join to  CSV File
Logger().log_and_slack("Step 9.  Writing Ouput CSV File.")

try:
    with open("tmp.csv", 'w', newline="\n") as csvfile:
        fieldnames = ['solr_search_term', 'ad_id']
        writer = csvOutPutFile.DictWriter(csvfile, fieldnames=fieldnames ,quotechar="'" )
        writer.writeheader()
        for excel_row in ExcelReady:
            writer.writerow(SolrSearchLogic().outout_row(NegativeProducts, excel_row))

        # Open the csv file to check how many rows there are
        with open("tmp.csv", "r", newline="\n") as f:
            reader = csvOutPutFile.reader(f,delimiter = ",")
            data = list(reader)
            row_count = len(data)
            Logger().log_and_slack("Step 9.1. CSV Output file has {} rows".format(row_count), '#adwords')

        Logger().log_and_slack("Step 10. CSV Output file : saved locally on drive. {}".format(CSVFileOutput), '#adwords')

        # replacing the tmp file with appopriate output file name and also replacing the required format double quotes
        f1 = open("tmp.csv", 'r')
        f2 = open((CSVFileOutput), 'w')
        for line in f1:
            f2.write(  ( ( (line.replace('",\'"', '","')).replace('"\'','"') ).replace('",""','","') ).replace('"","','","').replace('""','"') )
        f1.close()
        f2.close()

    f3 = "tmp.csv"
    path = os.path.dirname(os.path.abspath(f3) + "/" + f3)
    os.remove(path)
    os.remove(f3)
except OSError:
    pass
