import itertools
from lib.logger import Logger

class SolrSearchLogic:
    def convert_type_to_solr_syntax(self, value, type):
        if type == "Product":
            return ' +name_texts:"'+value+'"~4^4'
        elif type == "Boost2":
            return ' "'+value+'"^2'
        elif type == "Boost4":
            return ' "'+value+'"^4'
        elif type == "Required" :
            return ' +"'+value+'"'
        elif type == "Normal" :
            return ' "'+value+'"'
        elif type == "Colour":
            return ' +name_texts:"'+value+'"'

    def outout_row(self, NegativeProducts, excel_row):
        str = ""
        tmpp = self.str_ret_list(NegativeProducts, "AddNegP")
        str = str + (tmpp.rstrip()).lstrip()
        str= excel_row+"@"+ tmpp
        cellvalues = [x.strip() for x in str.split('@')]
        return {'solr_search_term': ''+cellvalues[2]+'', 'ad_id': '"'+cellvalues[1]+'"'}

    def str_ret_list(self, lst,negP):
        str = " "
        if (negP == "notnegP"):
            for v in lst[:-1]:
                str = str + v + ' '
            str = str + lst[-1]
        elif (negP == "AddNegP"):
            for v in lst[:-1]:
                str = str + '"' + (v.rstrip()).lstrip() + '", '
            str = str + '"' + (lst[-1]).rstrip().lstrip() + '"'
        elif (negP == "NegP"):
            str = ""
            for v in lst:
                str = str + ' -name_texts:"' + (v.rstrip()).lstrip() + '"'
        return str

    # function return the respective Type of the given value of search term
    def return_st_type(self, value, AllSearchTerms, AllSearchTermsTypes):
        i = 0
        for v in AllSearchTerms:
            if v == value :
                return AllSearchTermsTypes[i];
            else:
                i = i + 1

    # this function check the validity of the set if a given set has single value from all search term function
    def chValidSet(self, ss, SearchTerm1, SearchTerm2, SearchTerm3, SearchTerm4, Products):
        bool = True
        intersection_1 = set(ss).intersection(SearchTerm1)
        intersection_2 = set(ss).intersection(SearchTerm2)
        intersection_3 = set(ss).intersection(SearchTerm3)
        intersection_4 = set(ss).intersection(SearchTerm4)
        intersection_p = set(ss).intersection(Products)

        if self.check_if_seach_term_is_more_than_1_word(intersection_1):
            return False
        elif self.check_if_seach_term_is_more_than_1_word(intersection_2):
            return False
        elif self.check_if_seach_term_is_more_than_1_word(intersection_3):
            return False
        elif self.check_if_seach_term_is_more_than_1_word(intersection_4):
            return False
        elif self.check_if_seach_term_is_more_than_1_word(intersection_p):
            return False
        return bool

    def check_if_seach_term_is_more_than_1_word(self, search_term):
        search_term_list = ' '.join(search_term).split()
        return search_term.__len__() > 0 and search_term_list.__len__() > list(search_term)[0].split().__len__()

    def cross_join(self, AllSearchTerms, Products, rowCount, AllSearchTermsTypes, NegativeProducts, SearchTerm1, SearchTerm2, SearchTerm3, SearchTerm4, tmm):
        S = []
        sublist = []
        PComb = []
        ExcelReady = []
        itemsize = 0

        Logger().log_and_slack("Step 7.1. Creating Allitems complete SET and Set MAX size of total given items in all columns")
        for s in AllSearchTerms:
            S.append(itemsize)
            itemsize = itemsize + 1

        Logger().log_and_slack("Step 7.2. Finding total number of Subsets combination")
        def findsubsets(S,m):
            return set( itertools.combinations(S, m) )

        Logger().log_and_slack("Step 7.3. creating the subsets for differnt sizes added into a consolidated list")
        for i in range (1,6):
            Subset = findsubsets(S,i)
            for sub in Subset:
                for s in sub:
                    sublist.append(AllSearchTerms[s])
                PComb.append(sublist)
                sublist = []

        Logger().log_and_slack("Step 7.4. Initiating the cross join for the above consolidated subset list of sets")
        for P in Products:
            if not P :
                break;

            possible_combinations = len(PComb[::-1])
            Logger().log_and_slack(f"Possible number of combinations for '{P}': {possible_combinations} in the cross join")

            processed_count = 0

            for s in PComb[::-1]:
                if processed_count % 500000 == 0:
                    Logger().log_and_slack(f"Total combinations processed: {processed_count} out of a possible {possible_combinations}")

                if self.chValidSet(s, SearchTerm1, SearchTerm2, SearchTerm3, SearchTerm4, Products):
                    if s.__len__()   == 1:
                        Result = P + "@%d" % (rowCount) + "@" + SolrSearchLogic().convert_type_to_solr_syntax(s[0], self.return_st_type(s[0], AllSearchTerms, AllSearchTermsTypes)) + SolrSearchLogic().convert_type_to_solr_syntax(P,"Product") + SolrSearchLogic().str_ret_list(
                            NegativeProducts,"NegP") + "@" + SolrSearchLogic().str_ret_list(s+[P],"notnegP")
                        ExcelReady.append(Result)
                        rowCount = rowCount + 1
                    elif s.__len__() == 2:
                        Result = P + "@%d" % (rowCount) + "@" + SolrSearchLogic().convert_type_to_solr_syntax(s[0], self.return_st_type(s[0], AllSearchTerms, AllSearchTermsTypes)) + SolrSearchLogic().convert_type_to_solr_syntax(s[1], self.return_st_type(s[1], AllSearchTerms, AllSearchTermsTypes)
                                 ) + SolrSearchLogic().convert_type_to_solr_syntax(P, "Product") + SolrSearchLogic().str_ret_list(NegativeProducts,"NegP") + "@" + SolrSearchLogic().str_ret_list(s+[P],"notnegP")
                        ExcelReady.append(Result)
                        rowCount = rowCount + 1
                    elif s.__len__() == 3:
                        Result = P + "@%d" % (rowCount) + "@" + SolrSearchLogic().convert_type_to_solr_syntax(s[0], self.return_st_type(s[0], AllSearchTerms, AllSearchTermsTypes)) + SolrSearchLogic().convert_type_to_solr_syntax(s[1], self.return_st_type(s[1], AllSearchTerms, AllSearchTermsTypes)
                                  ) + SolrSearchLogic().convert_type_to_solr_syntax(s[2], self.return_st_type(s[2], AllSearchTerms, AllSearchTermsTypes) )+ SolrSearchLogic().convert_type_to_solr_syntax(P, "Product") + SolrSearchLogic().str_ret_list(NegativeProducts,"NegP"
                            ) + "@" + SolrSearchLogic().str_ret_list(s+[P],"notnegP")
                        ExcelReady.append(Result)
                        rowCount = rowCount + 1
                    elif s.__len__() == 4:
                        Result = P + "@%d" % (rowCount) + "@" + SolrSearchLogic().convert_type_to_solr_syntax(s[0], self.return_st_type(s[0], AllSearchTerms, AllSearchTermsTypes)) + SolrSearchLogic().convert_type_to_solr_syntax(s[1], self.return_st_type(s[1], AllSearchTerms, AllSearchTermsTypes)
                            ) + SolrSearchLogic().convert_type_to_solr_syntax(s[2], self.return_st_type(s[2], AllSearchTerms, AllSearchTermsTypes) ) + SolrSearchLogic().convert_type_to_solr_syntax(s[3], self.return_st_type(s[3], AllSearchTerms, AllSearchTermsTypes) )+ SolrSearchLogic().convert_type_to_solr_syntax(P, "Product"
                            ) + SolrSearchLogic().str_ret_list(NegativeProducts,"NegP") + "@" + SolrSearchLogic().str_ret_list(s+[P],"notnegP")
                        ExcelReady.append(Result)
                        rowCount = rowCount + 1
                    elif s.__len__() == 5:
                        Result = P + "@%d" % (rowCount) + "@" + SolrSearchLogic().convert_type_to_solr_syntax(s[0], self.return_st_type(s[0], AllSearchTerms, AllSearchTermsTypes)) + SolrSearchLogic().convert_type_to_solr_syntax(s[1], self.return_st_type(s[1], AllSearchTerms, AllSearchTermsTypes)
                                ) + SolrSearchLogic().convert_type_to_solr_syntax(s[2], self.return_st_type(s[2], AllSearchTerms, AllSearchTermsTypes) ) + SolrSearchLogic().convert_type_to_solr_syntax(s[3], self.return_st_type(s[3], AllSearchTerms, AllSearchTermsTypes) ) + SolrSearchLogic().convert_type_to_solr_syntax(s[4], self.return_st_type(s[4], AllSearchTerms, AllSearchTermsTypes)
                            ) + SolrSearchLogic().convert_type_to_solr_syntax(P,"Product") + SolrSearchLogic().str_ret_list(NegativeProducts,"NegP") + "@" + SolrSearchLogic().str_ret_list(s+[P],"notnegP")
                        ExcelReady.append(Result)
                        rowCount = rowCount + 1
                    elif s.__len__() == 6:
                        Result = P + "@%d" % (rowCount) + "@" + SolrSearchLogic().convert_type_to_solr_syntax(s[0], self.return_st_type(s[0], AllSearchTerms, AllSearchTermsTypes)) + SolrSearchLogic().convert_type_to_solr_syntax(s[1], self.return_st_type(s[1], AllSearchTerms, AllSearchTermsTypes)
                            ) + SolrSearchLogic().convert_type_to_solr_syntax(s[2], self.return_st_type(s[2], AllSearchTerms, AllSearchTermsTypes) ) + SolrSearchLogic().convert_type_to_solr_syntax(s[3], self.return_st_type(s[3], AllSearchTerms, AllSearchTermsTypes) ) + SolrSearchLogic().convert_type_to_solr_syntax(s[4],
                            self.return_st_type(s[4], AllSearchTerms )) + SolrSearchLogic().convert_type_to_solr_syntax(s[5], self.return_st_type(s[5], AllSearchTerms, AllSearchTermsTypes)) + SolrSearchLogic().convert_type_to_solr_syntax(P,"Product") + SolrSearchLogic().str_ret_list(NegativeProducts,"NegP"
                            ) + "@" + SolrSearchLogic().str_ret_list(s+[P],"notnegP")
                        ExcelReady.append(Result)
                        rowCount = rowCount + 1

                processed_count = processed_count + 1

        Logger().log_and_slack("Step 8.  Cross-Join Finished")
        return ExcelReady
