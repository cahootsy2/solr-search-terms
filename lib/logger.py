import slackweb
import logging

class Logger:
    def __init__(self):
        try:
            with open("solr_search_gen.log", "wt") as out_file:
                out_file.write("")
        finally:
            out_file.closed

    def log_and_slack(self, message, channel=None):
        logging.basicConfig(filename='solr_search_gen.log', level=logging.DEBUG, format='%(asctime)s - %(message)s')
        print(message)
        logging.info(message)
        if channel:
            self.slack().notify(text="SOLRSearchGenerator:{}".format(message), channel=channel,
                         username='cahootsy-bot', icon_emoji=':snowboarder:')
        self.slack().notify(text="SOLRSearchGenerator:{}".format(message), channel='#background-jobs',
                     username='cahootsy-bot', icon_emoji=':snowboarder:')

    def slack(self):
        return slackweb.Slack(url='https://hooks.slack.com/services/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')
