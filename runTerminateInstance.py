## Commented this logic out because we are not currently using S3

# #!/usr/bin/python3
#
# from boto.ec2.connection import EC2Connection
# import requests
# import boto
# import boto3
# import slackweb
# import logging
# import datetime
# import time
# import argparse
#
# # write message to the slack and logging with particular pattern
# def write_message(message):
#     print message
#     logging.info(message)
#     slack.notify(text="SOLR Termination: {}".format(message), channel='#background-jobs', username='cahootsy-bot',
#                  icon_emoji=':snowboarder:')
#
# # initializing types available sizes of images
# instance_types = ['t2.nano', 't2.micro', 't2.small', 't2.medium', 't2.large', 'm4.large', 'm4.xlarge', 'm4.2xlarge',
#                   'm4.4xlarge', 'm4.10xlarge', 'm3.medium', 'm3.large', 'm3.xlarge', 'm3.2xlarge', 'c4.large',
#                   'c4.xlarge', 'c4.2xlarge', 'c4.4xlarge', 'c4.8xlarge', 'c3.large', 'c3.xlarge', 'c3.2xlarge',
#                   'c3.4xlarge', 'c3.8xlarge', 'g2.2xlarge', 'g2.8xlarge', 'r3.large', 'r3.xlarge', 'r3.2xlarge',
#                   'r3.4xlarge', 'r3.8xlarge', 'i2.xlarge', 'i2.2xlarge', 'i2.4xlarge', 'i2.8xlarge', 'd2.xlarge',
#                   'd2.2xlarge', 'd2.4xlarge', 'd2.8xlarge']
#
# # initializing slack which will be used to send output to later be visible to running instance
# slack = slackweb.Slack(url='https://hooks.slack.com/services/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')
#
# # providing runTerminateInstance.log file name which will be storing the log information
# logging.basicConfig(filename='runTerminateInstance.log', level=logging.DEBUG, format='%(asctime)s - %(message)s')
#
#
# # initializing variables e.g. ec2 and ec2/s3 clients which will later be used to access ec2 and s3 storage
# boto3.setup_default_session(region_name='eu-west-1')
# s3 = boto3.resource('s3')
# s3_client = boto3.client('s3')
# ec2 = boto3.resource('ec2')
# ec2_client = boto3.client('ec2')
#
# # compare two values and return appropriate result to the caller function
# def compValues(v1,v2):
#     flag = False
#     if not v1:
#         flag = False
#     elif not v2:
#         flag = False
#     elif v1 == v2:
#         flag = True
#     elif v1 != v2:
#         flag = False
#     return flag
#
# # This function parse the TimeZone format and extract the required information to send back to caller function
# def timetrunc(tmp):
#     timepatrn=''
#     values = [x.strip() for x in tmp.split('T')]
#     i = 0
#     for v in values:
#         if i == 0:
#             timepatrn = v
#             timepatrn = timepatrn + " "
#         elif i == 1:
#             timepatrn = timepatrn + v[0:-5]
#             timepatrn = timepatrn
#         i = i + 1
#     return timepatrn
#
# # This function return str format of the time and truncate trailing microsend from the time format
# def strtime(str):
#     tmp = str.__str__()
#     return tmp[0:-7]
#
# # This function call parse the instance string (e.g. [ instance:1-abfdg444] )  for ID and returned to caller
# def parseInstanceString(str):
#     sub=""
#     sub = str.__str__() + ""
#     tmp = sub[1:-1]
#     sub = tmp.split(':')
#     return sub[1]
#
# def parse_arguments():
#     parser = argparse.ArgumentParser(description='Terminate a SOLR instance of Product Categorization')
#
#     parser.add_argument('--instanceid', help='the instance id of new EC3 server (e.g. i-445fef3)', default='none')
#
#     return parser.parse_args()
#
# # This is the MAIN of the program where we are calling each function to get required information
# # set region and get all reservations and look for at least one reservation to proceed
# # we get the list of instances for respective reservation and loop through that list to get the information for interested
# # instance and terminate that one
# # the logic here used to terminate is if instance is of given type r4.large and running state and elapse time is less
# # than 80mins
#
# try:
#     args = parse_arguments()
#     conn = boto.ec2.connect_to_region("eu-west-1")
#     reservations = conn.get_all_reservations()
#     if reservations.__len__ > 0:
#         lenn = len(reservations)
#         for v in range(1,lenn):
#             instance = reservations[v].instances
#             for inst in instance:
#                 tmp = inst.launch_time
#                 dd1 = timetrunc(tmp)
#                 dd2 = strtime(datetime.datetime.now())
#                 d1 = datetime.datetime.strptime(dd1, '%Y-%m-%d %H:%M:%S')
#                 d2 = datetime.datetime.strptime(dd2, '%Y-%m-%d %H:%M:%S')
#                 # convert to unix timestamp
#                 d1_ts = time.mktime(d1.timetuple())
#                 d2_ts = time.mktime(d2.timetuple())
#                 # they are now in seconds, subtract and then divide by 60 to get minutes.
#                 timediff = int(d2_ts - d1_ts) / (60)
#                 instanceID = parseInstanceString(instance)
#                 if compValues(inst.instance_type,"c4.large") and compValues(inst.state,"running"
#                     ) and not (inst.spot_instance_request_id is None) and args.instanceid == instanceID :
#                     write_message("Terminating instance {}".format(instance))
#                     write_message("Instance Type: {}".format(inst.instance_type))
#                     write_message("Instance Launch Time: {}".format(inst.launch_time))
#                     write_message("Instance Spot Instance Request Id: {}".format(inst.spot_instance_request_id))
#                     write_message("Instance Elapsed Time: {}".format(timediff))
#                     write_message("Instance Current State: {}".format(inst.state))
#                     write_message("Instance ID : {}".format(instanceID))
#                     ec2_client.terminate_instances(InstanceIds=[instanceID])
#                     write_message("Sucessfully Terminated instance {}".format(instanceID))
#
# finally:
#     write_message("Termination Complete.")
