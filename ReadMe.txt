# Solr Search Term Generator

## Description
This program is written in python 3.3 and it is assumed that the following libraries are already added to python library section, which are essential for successful run of this program.

import requests     // Used for internet access to google spreadsheet
import csv          // Manuplulate CSV files 
import io           // Read / Write Access System files
import os			// Delete File from system
import itertools    // Create subsets combinations for given set

If above libraries are not set primarily in the python setup then you can add them from below python library download link.

e.g. https://pypi.python.org/pypi

The code is built in such way that it is pretty self explanatory and as the code executes it displays each section / step excution.

Mainly the code has the following sections or steps which are traversed during the execution and also helps in debugging the code e.g. if code is not responding or takes longer at particular step then you can have the step number and go to that particular step for further review of the code.

The user can set the Input CSV URL as well as the target URL in local text file named input_url.txt, for MAX ad_id we are reading it from sample source_Output.csv provided in the input_url.txt as the code searches 2nd column until the last row to extract value and start incrementing from that value for NEW "source_Output.csv" file.
here are the two variables which can be set in the TXT file (input_url.txt) for these two URLs.

url_InputSource=https://docs.google.com/spreadsheets/d/1LspvJVaNHbOE8_AWGBaIMB4mmiw5i1CbBKsI640J1Ic/
url_MaxID=https://docs.google.com/spreadsheets/d/17DiVgPK0LtDLbpggfxGa0dNSiMXmwfTIDVs2s-HoPos/

We have introduced set concept to find exact number of combination for given number of sheet columns (Tables) where each one contains multiple values. Just to give you more insight into how total number of combinations are adding up. here is the math.
In sample input_url total number of columns (7) with each one containing values (13) are enclosed in brackets as following: (Product(1),AllSearchTerms(2),SearchTerm1(3),SearchTerm2(5),SearchTerm3(3),SearchTerm4(1),Negative_Products(1))
Total Combinations and mathematical representation:

		   13       13       13        13       13        13
			C   +    C    +   C   +     C    +   C     +    C
			 6        5       4          3        2          1
	-------------------------------------------------------------------
Counts =  1716  +   1287  +   715   +  286  +  78   +     13    = Sum = 4095

Here is how we calculate above expression, here we will do for one and rest can be done similarly [ ! means (factorial) ].
13            13!               13x12x11x10x9x8x7x6x5x4x3x2x1 
  C     =  ----------     =   ----------------------------------  =   1716
   6         (13-6)! 6!          7x6x5x4x3x2x1   X  6x5x4x3x2x1
	
To explain a little more the above express means total number of words/strings/variables available for position of 6 words combinations with no repititions allowed.

Here is sample output format that is generated in source_urls.csv
Product	ad_id	solr_search_term	KeyWord	 Negative_Products
Dress,40179,"Summer"^2 "Adaptation"^2 angora antimicrobial +"Black" +"Red" +name_texts:"Dress"~4^4 -"fancy dress" -" dress shirt",Summer Adaptation angora antimicrobial Black Red Dress,'"fancy dress", "dress shirt"'
Dress,40180,"Summer"^2 "Allover"^2 angora boucle denim +"Beige" +name_texts:"Dress"~4^4 -"fancy dress" -" dress shirt",Summer Allover angora boucle denim Beige Dress,'"fancy dress", "dress shirt"'

## Testing
To run the unit tests for this project:
`$ python test-solr-search-gen.py`

## Development
In development you should be working off of the `develop` branch.

## Production
When you are ready to commit your changes to production you can merge your changes into `master`.

## Known issues
* This script does not scale particularly well with large input csv files. In particular step 7.3 tends to get stuch with large input csvs. I believe this is due `itertools.combinations(S, m)`.
* When this script was running in AWS is was even worse at handling large input csv files. This may have been due to a memory leak or it could have been the same issue as above
