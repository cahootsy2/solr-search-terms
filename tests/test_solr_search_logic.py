import unittest
from context import SolrSearchLogic

class ConvertTypeToSolrSyntaxTests(unittest.TestCase):
    def testProduct(self):
        self.assertEqual(SolrSearchLogic().convert_type_to_solr_syntax('long dress', 'Product'), ' +name_texts:"long dress"~4^4')

    def testBoost2(self):
        self.assertEqual(SolrSearchLogic().convert_type_to_solr_syntax('long dress', 'Boost2'), ' "long dress"^2')

    def testBoost4(self):
        self.assertEqual(SolrSearchLogic().convert_type_to_solr_syntax('long dress', 'Boost4'), ' "long dress"^4')

    def testRequired(self):
        self.assertEqual(SolrSearchLogic().convert_type_to_solr_syntax('long dress', 'Required'), ' +"long dress"')

    def testNormal(self):
        self.assertEqual(SolrSearchLogic().convert_type_to_solr_syntax('long dress', 'Normal'), ' "long dress"')

    def testColour(self):
        self.assertEqual(SolrSearchLogic().convert_type_to_solr_syntax('long dress', 'Colour'), ' +name_texts:"long dress"')

class OutputRowTests(unittest.TestCase):
    def testOutputRow(self):
        NegativeProducts = ['fancy dress', ' dress shirt']
        excel_row = 'Shirt@44880@ +"value2" +name_texts:"Shirt"~4^4 -"fancy dress" -"dress shirt"@ value2 Shirt'
        result = {'solr_search_term': '+"value2" +name_texts:"Shirt"~4^4 -"fancy dress" -"dress shirt"', 'ad_id': '"44880"'}
        self.assertEqual(SolrSearchLogic().outout_row(NegativeProducts, excel_row), result)

class strRetListTest(unittest.TestCase):
    def test_not_neg_p(self):
        list = ['fancy dress', ' dress shirt']
        self.assertEqual(SolrSearchLogic().str_ret_list(list, 'notnegP'), ' fancy dress  dress shirt')

    def test_add_neg_p(self):
        list = ['fancy dress', ' dress shirt']
        self.assertEqual(SolrSearchLogic().str_ret_list(list, 'AddNegP'), ' "fancy dress", "dress shirt"')

    def test_neg_p(self):
        list = ['fancy dress', ' dress shirt']
        self.assertEqual(SolrSearchLogic().str_ret_list(list, 'NegP'), ' -name_texts:"fancy dress" -name_texts:"dress shirt"')

class CheckIfSingleSearchTermTest(unittest.TestCase):
    def test_single_word_term(self):
        search_term = set(["dress"])
        self.assertEqual(SolrSearchLogic().check_if_seach_term_is_more_than_1_word(search_term), False)

    def test_double_word_term(self):
        search_term = set(["gold dress"])
        self.assertEqual(SolrSearchLogic().check_if_seach_term_is_more_than_1_word(search_term), False)

    def test_two_words_term(self):
        search_term = set(["green", "dress"])
        self.assertEqual(SolrSearchLogic().check_if_seach_term_is_more_than_1_word(search_term), True)

class CrossJoinTest(unittest.TestCase):
    def test_cross_join(self):
        Products = ['Dress', 'Shirt']
        rowCount = 40179
        tmm = '40179'
        SearchTerm1 = ['Summer', 'Strappy', 'value9', 'value1', 'value10', 'value11']
        SearchTerm2 = ['Adaptation', 'Airbrush', 'Allover', 'value3', 'value8', 'value12', 'value14']
        SearchTerm3 = ['angora', 'antimicrobial', 'boucle', 'canvas', 'denim']
        SearchTerm4 = ['Beige', 'Black', 'Red', 'value2', 'value7', 'value13']
        AllSearchTerms = SearchTerm1 + SearchTerm2 + SearchTerm3 + SearchTerm4
        AllSearchTermsTypes = ['Boost2', 'Boost2', 'Boost2', 'Boost2', 'Boost2', 'Boost2', 'Boost4', 'Boost4', 'Boost4', 'Boost4', 'Boost4', 'Boost4', 'Boost4', 'Normal', 'Normal', 'Normal', 'Normal', 'Normal', 'Required', 'Required', 'Required', 'Required', 'Required', 'Required']
        NegativeProducts = ['fancy dress', ' dress shirt      ']
        ExcelReady = SolrSearchLogic().cross_join(AllSearchTerms, Products, rowCount, AllSearchTermsTypes, NegativeProducts, SearchTerm1, SearchTerm2, SearchTerm3, SearchTerm4, tmm)

        self.assertEqual(len(ExcelReady), 4702)
        self.assertEqual(ExcelReady[0], 'Dress@40179@ "value9"^2 "value14"^4 "angora" +"Red" +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ value9 value14 angora Red Dress')

    def test_cross_join_with_space_in_keywords(self):
        Products = ['Dress']
        rowCount = 40179 # TODO: this should be generated not passed in
        tmm = '40179' # TODO: this should be generated not passed in
        SearchTerm1 = ['Summer', 'Big Straps']
        SearchTerm2 = ['Adaptation', 'Airbrush']
        SearchTerm3 = []
        SearchTerm4 = []
        AllSearchTerms = SearchTerm1 + SearchTerm2
        AllSearchTermsTypes = ['Required', 'Required', 'Boost4', 'Boost4']
        NegativeProducts = ['fancy dress', ' dress shirt      ']
        ExcelReady = SolrSearchLogic().cross_join(AllSearchTerms, Products, rowCount, AllSearchTermsTypes, NegativeProducts, SearchTerm1, SearchTerm2, SearchTerm3, SearchTerm4, tmm)

        self.assertEqual(len(ExcelReady), 8)
        self.assertEqual(ExcelReady[0], 'Dress@40179@ +"Summer" "Adaptation"^4 +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Summer Adaptation Dress')
        self.assertEqual(ExcelReady[1], 'Dress@40180@ +"Summer" "Airbrush"^4 +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Summer Airbrush Dress')
        self.assertEqual(ExcelReady[2], 'Dress@40181@ +"Big Straps" "Airbrush"^4 +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Big Straps Airbrush Dress')
        self.assertEqual(ExcelReady[3], 'Dress@40182@ +"Big Straps" "Adaptation"^4 +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Big Straps Adaptation Dress')
        self.assertEqual(ExcelReady[4], 'Dress@40183@ +"Big Straps" +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Big Straps Dress')
        self.assertEqual(ExcelReady[5], 'Dress@40184@ "Airbrush"^4 +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Airbrush Dress')
        self.assertEqual(ExcelReady[6], 'Dress@40185@ +"Summer" +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Summer Dress')
        self.assertEqual(ExcelReady[7], 'Dress@40186@ "Adaptation"^4 +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Adaptation Dress')

    def test_cross_join_with_4_keywords(self):
        Products = ['Dress']
        rowCount = 40179 # TODO: this should be generated not passed in
        tmm = '40179' # TODO: this should be generated not passed in
        SearchTerm1 = ['Summer', 'Strappy']
        SearchTerm2 = ['Adaptation', 'Airbrush']
        SearchTerm3 = []
        SearchTerm4 = []
        AllSearchTerms = SearchTerm1 + SearchTerm2
        AllSearchTermsTypes = ['Required', 'Required', 'Boost4', 'Boost4']
        NegativeProducts = ['fancy dress', ' dress shirt      ']
        ExcelReady = SolrSearchLogic().cross_join(AllSearchTerms, Products, rowCount, AllSearchTermsTypes, NegativeProducts, SearchTerm1, SearchTerm2, SearchTerm3, SearchTerm4, tmm)

        self.assertEqual(len(ExcelReady), 8)
        self.assertEqual(ExcelReady[0], 'Dress@40179@ +"Summer" "Adaptation"^4 +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Summer Adaptation Dress')
        self.assertEqual(ExcelReady[1], 'Dress@40180@ +"Summer" "Airbrush"^4 +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Summer Airbrush Dress')
        self.assertEqual(ExcelReady[2], 'Dress@40181@ +"Strappy" "Airbrush"^4 +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Strappy Airbrush Dress')
        self.assertEqual(ExcelReady[3], 'Dress@40182@ +"Strappy" "Adaptation"^4 +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Strappy Adaptation Dress')
        self.assertEqual(ExcelReady[4], 'Dress@40183@ +"Strappy" +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Strappy Dress')
        self.assertEqual(ExcelReady[5], 'Dress@40184@ "Airbrush"^4 +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Airbrush Dress')
        self.assertEqual(ExcelReady[6], 'Dress@40185@ +"Summer" +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Summer Dress')
        self.assertEqual(ExcelReady[7], 'Dress@40186@ "Adaptation"^4 +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Adaptation Dress')

    def test_cross_join_with_5_keywords(self):
        Products = ['Dress']
        rowCount = 40179
        tmm = '40179'
        SearchTerm1 = ['Summer', 'Strappy']
        SearchTerm2 = ['Adaptation', 'Airbrush']
        SearchTerm3 = ['Bright']
        SearchTerm4 = []
        AllSearchTerms = SearchTerm1 + SearchTerm2 + SearchTerm3
        AllSearchTermsTypes = ['Required', 'Required', 'Boost4', 'Boost4', 'Normal']
        NegativeProducts = ['fancy dress', ' dress shirt      ']
        ExcelReady = SolrSearchLogic().cross_join(AllSearchTerms, Products, rowCount, AllSearchTermsTypes, NegativeProducts, SearchTerm1, SearchTerm2, SearchTerm3, SearchTerm4, tmm)

        self.assertEqual(len(ExcelReady), 17)
        self.assertEqual(ExcelReady[0], 'Dress@40179@ +"Summer" "Adaptation"^4 "Bright" +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Summer Adaptation Bright Dress')
        self.assertEqual(ExcelReady[1], 'Dress@40180@ +"Strappy" "Adaptation"^4 "Bright" +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Strappy Adaptation Bright Dress')
        self.assertEqual(ExcelReady[2], 'Dress@40181@ +"Strappy" "Airbrush"^4 "Bright" +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Strappy Airbrush Bright Dress')
        self.assertEqual(ExcelReady[3], 'Dress@40182@ +"Summer" "Airbrush"^4 "Bright" +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Summer Airbrush Bright Dress')
        self.assertEqual(ExcelReady[4], 'Dress@40183@ +"Summer" "Adaptation"^4 +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Summer Adaptation Dress')
        self.assertEqual(ExcelReady[5], 'Dress@40184@ "Airbrush"^4 "Bright" +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Airbrush Bright Dress')
        self.assertEqual(ExcelReady[6], 'Dress@40185@ +"Summer" "Airbrush"^4 +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Summer Airbrush Dress')
        self.assertEqual(ExcelReady[7], 'Dress@40186@ +"Summer" "Bright" +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Summer Bright Dress')
        self.assertEqual(ExcelReady[8], 'Dress@40187@ "Adaptation"^4 "Bright" +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Adaptation Bright Dress')
        self.assertEqual(ExcelReady[9], 'Dress@40188@ +"Strappy" "Bright" +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Strappy Bright Dress')
        self.assertEqual(ExcelReady[10], 'Dress@40189@ +"Strappy" "Airbrush"^4 +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Strappy Airbrush Dress')
        self.assertEqual(ExcelReady[11], 'Dress@40190@ +"Strappy" "Adaptation"^4 +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Strappy Adaptation Dress')
        self.assertEqual(ExcelReady[12], 'Dress@40191@ "Bright" +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Bright Dress')
        self.assertEqual(ExcelReady[13], 'Dress@40192@ "Airbrush"^4 +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Airbrush Dress')
        self.assertEqual(ExcelReady[14], 'Dress@40193@ "Adaptation"^4 +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Adaptation Dress')
        self.assertEqual(ExcelReady[15], 'Dress@40194@ +"Strappy" +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Strappy Dress')
        self.assertEqual(ExcelReady[16], 'Dress@40195@ +"Summer" +name_texts:"Dress"~4^4 -name_texts:"fancy dress" -name_texts:"dress shirt"@ Summer Dress')

def main():
    unittest.main()

if __name__ == '__main__':
    main()
